use derive_more::From;
use serde::Serialize;

use super::scheme;

pub type Result<T> = core::result::Result<T, Error>;

#[derive(Debug, Serialize, From)]
pub enum Error {
    PwdWithSchemeFailedParse,

    FailSpawnBlockForValidate,
    FailSpawnBlockForHash,

    // Modules
    #[from]
    Scheme(scheme::Error),
}

//impl From<module::Error> for Error {
//     fn from(value: module::Error) -> Self {
//         Self::Variant(value)
//     }
//}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{self:?}")
    }
}

impl std::error::Error for Error {}
