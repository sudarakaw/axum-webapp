use serde::Serialize;

pub type Result<T> = core::result::Result<T, Error>;

#[derive(Debug, Serialize)]
pub enum Error {
    // Key
    HmacFailNewFromSlice,

    // Token
    InvlidFormat,
    CannotDecodeIndentity,
    CannotDecodeExpiration,
    SignatureNotMatching,
    ExpirationNotIso,
    Expired,
}

//impl From<module::Error> for Error {
//     fn from(value: module::Error) -> Self {
//         Self::Variant(value)
//     }
//}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{self:?}")
    }
}

impl std::error::Error for Error {}
