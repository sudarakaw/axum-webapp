mod error;

use std::{fmt::Display, str::FromStr};

use hmac::{Hmac, Mac};
use lib_utils::{
    b64::{b64u_decode_to_string, b64u_encode},
    time::{now_utc, now_utc_plus_sec_str, parse_utc},
};
use sha2::Sha512;
use uuid::Uuid;

use crate::config::config;

pub use self::error::{Error, Result};

// {{{ Token Type

/// String format: `identity_b64u.expitarion_b64u.signature_b64u`
#[derive(Debug)]
#[cfg_attr(test, derive(PartialEq))]
pub struct Token {
    pub identity: String,       // Identifier (username, user id, ect.)
    pub expiration: String,     // Expiration date in Rfc3399 format.
    pub signature_b64u: String, // Signature (base64_url encoded)
}

impl FromStr for Token {
    type Err = Error;

    fn from_str(token_str: &str) -> std::prelude::v1::Result<Self, Self::Err> {
        let splits: Vec<&str> = token_str.split('.').collect();

        if 3 != splits.len() {
            return Err(Error::InvlidFormat);
        }

        let (identity_b64u, expiration_b64u, signature_b64u) = (splits[0], splits[1], splits[2]);

        Ok(Self {
            identity: b64u_decode_to_string(identity_b64u)
                .map_err(|_| Error::CannotDecodeIndentity)?,
            expiration: b64u_decode_to_string(expiration_b64u)
                .map_err(|_| Error::CannotDecodeExpiration)?,
            signature_b64u: signature_b64u.to_string(),
        })
    }
}

impl Display for Token {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}.{}.{}",
            b64u_encode(&self.identity),
            b64u_encode(&self.expiration),
            self.signature_b64u
        )
    }
}

// }}}

// {{{ Web Token generation & validation

pub fn generate_web_token(user: &str, salt: Uuid) -> Result<Token> {
    let config = &config();

    _generate_token(user, config.TOKEN_DURATION_SEC, salt, &config.TOKEN_KEY)
}

pub fn validate_web_token(origin_token: &Token, salt: Uuid) -> Result<()> {
    let config = &config();

    _validate_token_signature_and_expiration(origin_token, salt, &config.TOKEN_KEY)?;

    Ok(())
}

// }}}

// {{{ (private) Token generation & validation

fn _generate_token(identity: &str, duration_sec: f64, salt: Uuid, key: &[u8]) -> Result<Token> {
    // Compute first two componenets
    let identity = identity.to_string();
    let expiration = now_utc_plus_sec_str(duration_sec);

    // Sign the first two components
    let signature_b64u = _token_sign_into_b64u(&identity, &expiration, salt, key)?;

    Ok(Token {
        identity,
        expiration,
        signature_b64u,
    })
}

fn _validate_token_signature_and_expiration(
    origin_token: &Token,
    salt: Uuid,
    key: &[u8],
) -> Result<()> {
    // Validate signature
    let new_signature_b64u =
        _token_sign_into_b64u(&origin_token.identity, &origin_token.expiration, salt, key)?;

    if new_signature_b64u != origin_token.signature_b64u {
        return Err(Error::SignatureNotMatching);
    }

    // Validate expiration
    let origin_expiration =
        parse_utc(&origin_token.expiration).map_err(|_| Error::ExpirationNotIso)?;
    let now = now_utc();

    if origin_expiration < now {
        return Err(Error::Expired);
    }

    Ok(())
}

/// Create token signature from token parts and salt.
fn _token_sign_into_b64u(
    identity: &str,
    expiration: &str,
    salt: Uuid,
    key: &[u8],
) -> Result<String> {
    let content = format!("{}.{}", b64u_encode(identity), b64u_encode(expiration));

    // Create a HMAC-SHA-512 from key.
    let mut hmac_sha512 =
        Hmac::<Sha512>::new_from_slice(key).map_err(|_| Error::HmacFailNewFromSlice)?;

    // Add content.
    hmac_sha512.update(content.as_bytes());
    hmac_sha512.update(salt.as_bytes());

    // Finalize and b64u encode.
    let hmac_result = hmac_sha512.finalize();
    let result_bytes = hmac_result.into_bytes();
    let result = b64u_encode(result_bytes);

    Ok(result)
}

// }}}

// {{{

#[cfg(test)]
mod tests {
    use std::{thread, time::Duration};

    use anyhow::Result;

    use super::*;

    #[test]
    fn test_token_display_ok() -> Result<()> {
        // Fixtures
        let fx_token = Token {
            identity: "fx-ident-01".to_string(),
            expiration: "2024-01-25T14:12:00Z".to_string(),
            signature_b64u: "some-signature-b64u-encoded".to_string(),
        };
        let fx_token_str =
            "ZngtaWRlbnQtMDE.MjAyNC0wMS0yNVQxNDoxMjowMFo.some-signature-b64u-encoded";

        // Check
        assert_eq!(fx_token.to_string(), fx_token_str);

        Ok(())
    }

    #[test]
    fn test_token_from_str_ok() -> Result<()> {
        // Fixtures
        let fx_token = Token {
            identity: "fx-ident-01".to_string(),
            expiration: "2024-01-25T14:12:00Z".to_string(),
            signature_b64u: "some-signature-b64u-encoded".to_string(),
        };
        let fx_token_str =
            "ZngtaWRlbnQtMDE.MjAyNC0wMS0yNVQxNDoxMjowMFo.some-signature-b64u-encoded";

        // Exec
        let token: Token = fx_token_str.parse()?;

        // Check
        assert_eq!(token, fx_token);

        Ok(())
    }

    #[test]
    fn test_validate_web_token_ok() -> Result<()> {
        // Setup & Fixtures
        let fx_user = "user_one";
        let fx_salt = Uuid::parse_str("f05e8961-d6ad-4086-9e78-a6de065e5453").unwrap();
        let fx_duration_sec = 0.02; // 20ms
        let token_key = &config().TOKEN_KEY;
        let fx_token = _generate_token(fx_user, fx_duration_sec, fx_salt, token_key)?;

        // Exec
        thread::sleep(Duration::from_millis(10));
        let res = validate_web_token(&fx_token, fx_salt);

        // Check
        res?;

        Ok(())
    }

    #[test]
    fn test_validate_web_token_err_expired() -> Result<()> {
        // Setup & Fixtures
        let fx_user = "user_one";
        let fx_salt = Uuid::parse_str("f05e8961-d6ad-4086-9e78-a6de065e5453").unwrap();
        let fx_duration_sec = 0.01; // 20ms
        let token_key = &config().TOKEN_KEY;
        let fx_token = _generate_token(fx_user, fx_duration_sec, fx_salt, token_key)?;

        // Exec
        thread::sleep(Duration::from_millis(20));
        let res = validate_web_token(&fx_token, fx_salt);

        // Check
        assert!(
            matches!(res, Err(Error::Expired)),
            "Should have matched `Err(Error::Expired)` but was {res:?}"
        );

        Ok(())
    }
}

// }}}
