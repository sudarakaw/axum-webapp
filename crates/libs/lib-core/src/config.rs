use std::sync::OnceLock;

use lib_utils::env::{get_env, Result};

pub fn config() -> &'static Config {
    static INSTANCE: OnceLock<Config> = OnceLock::new();

    INSTANCE.get_or_init(|| {
        Config::load_from_env().unwrap_or_else(|e| {
            panic!("FATAL - WHILE LOADING CONFIG - Cause: {e:?}");
        })
    })
}

#[allow(non_snake_case)]
pub struct Config {
    // Db
    pub DB_URL: String,
}

impl Config {
    fn load_from_env() -> Result<Self> {
        Ok(Self {
            // Db
            DB_URL: get_env("SERVICE_DB_URL")?,
        })
    }
}
