//! Model Layer
//!
//! Design:
//!
//! - The Model layer normalizes the applications's data types structures and
//!   access.
//! - All application code data access must go through the Model layer.
//! - The `ModelManager` holds the internal states/resources needed by
//!   ModelController to access data.
//!   (e.g., db_rool, S3 client, redis client).
//! - Model Controllers (e.g., `TaskBmc`, 'ProjectBmc`) implement CRUD and other
//!   data access methods on a given "entity"
//!   (e.g., `Task`, `Project`).
//!   (`Bmc` is short for Backend Model Controller).
//! - In frameworks like Axum, Tauri, `ModelManager` are typically used as App
//!   State.
//! - ModelManager are designed to be passed as an argument to all Model
//!   Controller functions.
//!

mod base;
mod error;
mod store;

pub mod task;
pub mod user;

pub use self::error::{Error, Result};
use self::store::{new_db_pool, Db};

#[derive(Clone)]
pub struct ModelManager {
    db: Db,
}

// Constructor
impl ModelManager {
    pub async fn new() -> Result<Self> {
        let db = new_db_pool().await?;

        Ok(Self { db })
    }

    /// Returns the sqlx db pool recerence.
    /// (Only to the model layer)
    pub(in crate::model) fn db(&self) -> &Db {
        &self.db
    }
}
