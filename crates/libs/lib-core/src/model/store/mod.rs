use sqlx::{postgres::PgPoolOptions, Pool, Postgres};

use crate::config::config;

pub use self::error::Error;
use self::error::Result;

mod error;

pub type Db = Pool<Postgres>;

pub async fn new_db_pool() -> Result<Db> {
    // See https://github.com/rust10x/rust-web-app/commit/7273386d6513cd8dccfcd3ee1d25608fb9947cc5#diff-6db4e45d5f4ed6c486f906491f05a68fdd64a745074340db976829008476bbce
    let max_connections = if cfg!(test) { 1 } else { 5 };

    PgPoolOptions::new()
        .max_connections(max_connections)
        .connect(&config().DB_URL)
        .await
        .map_err(|e| Error::FailedToCreatePool(e.to_string()))
}
