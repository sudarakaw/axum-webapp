use derive_more::From;
use serde::Serialize;
use serde_with::{serde_as, DisplayFromStr};

use lib_core::model;

pub type Result<T> = core::result::Result<T, Error>;

#[serde_as]
#[derive(Debug, Serialize, From)]
pub enum Error {
    MethodUnknown(String),
    MissingParams {
        method: String,
    },
    FailJsonParams {
        method: String,
    },

    // Modules
    #[from]
    Model(model::Error),

    // External Modules
    #[from]
    SerdeJson(#[serde_as(as = "DisplayFromStr")] serde_json::Error),
}

//impl From<module::Error> for Error {
//     fn from(value: module::Error) -> Self {
//         Self::Variant(value)
//     }
//}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{self:?}")
    }
}

impl std::error::Error for Error {}
