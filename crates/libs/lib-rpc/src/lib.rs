mod error;
mod params;
mod task;

use lib_core::ctx::Ctx;
use serde::Deserialize;
use serde_json::{from_value, to_value, Value};

use lib_core::model::ModelManager;

pub use self::error::{Error, Result};

// The raw JSON-RPC request object, serving as the foundation for RPC routing.
#[derive(Deserialize)]
pub struct RpcRequest {
    pub id: Option<Value>,
    pub method: String,
    pub params: Option<Value>,
}

/// RPC basic information containing the RPC request id and method for
/// additional logging purposes.
#[derive(Debug, Clone)]
pub struct RpcInfo {
    pub id: Option<Value>,
    pub method: String,
}

macro_rules! exec_rpc_fn {
    // Without Params
    ($rpc_fn:expr, $ctx:expr, $mm:expr) => {
        $rpc_fn($ctx, $mm).await.map(to_value)??
    };

    // With Params
    ($rpc_fn:expr, $ctx:expr, $mm:expr, $params:expr) => {{
        let fn_name = stringify!($rpc_fn);

        let params = $params.ok_or(Error::MissingParams {
            method: fn_name.to_string(),
        })?;
        let params = from_value(params).map_err(|_| Error::FailJsonParams {
            method: fn_name.to_string(),
        })?;

        $rpc_fn($ctx, $mm, params).await.map(to_value)??
    }};
}

pub async fn execute_rpc(mm: ModelManager, ctx: Ctx, req: RpcRequest) -> Result<Value> {
    let RpcRequest {
        id: _,
        method,
        params,
    } = req;

    // Execute & store RpcInfo in response
    let result_json: Value = match method.as_str() {
        // Task RPC methods.
        "create_task" => exec_rpc_fn!(task::create, ctx, mm, params),
        "list_tasks" => exec_rpc_fn!(task::list, ctx, mm, params),
        "get_task" => exec_rpc_fn!(task::get, ctx, mm, params),
        "update_task" => exec_rpc_fn!(task::update, ctx, mm, params),
        "delete_task" => exec_rpc_fn!(task::delete, ctx, mm, params),

        // Fall back as Err
        _ => return Err(Error::MethodUnknown(method)),
    };

    Ok(result_json)
}
