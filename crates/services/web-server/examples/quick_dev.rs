use anyhow::Result;
use serde_json::json;

#[tokio::main]
async fn main() -> Result<()> {
    let client = httpc_test::new_client("http://127.0.0.1:5000")?;

    // client.do_get("/index.html").await?.print().await?;

    let request_login = client.do_post(
        "/api/login",
        json!({
            "username": "demo1",
            "password": "welcome"
        }),
    );

    request_login.await?.print().await?;

    // Create Tasks
    let mut task_ids: Vec<i64> = Vec::new();

    for i in 0..5 {
        let request_create_task = client.do_post(
            "/api/rpc",
            json!({
                "id": 1,
                "method": "create_task",
                "params": {
                    "data": {
                        "title": format!("task AAA {i}")
                    }
                }
            }),
        );
        let result = request_create_task.await?;
        task_ids.push(result.json_value("/result/id")?);
    }

    // Update first task
    let request_update_task = client.do_post(
        "/api/rpc",
        json!({
            "id": 1,
            "method": "update_task",
            "params": {
                "id": task_ids[0],
                "data": {
                    "title":"task BB"
                }
            }
        }),
    );
    request_update_task.await?.print().await?;

    // Delete second Task
    let request_delete_task = client.do_post(
        "/api/rpc",
        json!({
            "id": 1,
            "method": "delete_task",
            "params": {
                "id": task_ids[1]
            }
        }),
    );
    request_delete_task.await?.print().await?;

    let request_list_task = client.do_post(
        "/api/rpc",
        json!({
            "id": 1,
            "method": "list_tasks",
            "params": {
                "filters": [ {
                    "title": { "$endsWith": "BB" }
                }, {
                    "id": { "$in": [ task_ids[2], task_ids[3] ] }
                } ],
                "list_options": {
                    "order_bys": "!id"
                }
            }
        }),
    );
    request_list_task.await?.print().await?;

    // let request_logoff = client.do_post(
    //     "/api/logoff",
    //     json!({
    //         "logoff": true
    //     }),
    // );
    //
    // request_logoff.await?.print().await?;

    Ok(())
}
