use std::sync::OnceLock;

use lib_utils::env::{get_env, get_env_b64u_as_u8s, get_env_parse, Result};

pub fn config() -> &'static Config {
    static INSTANCE: OnceLock<Config> = OnceLock::new();

    INSTANCE.get_or_init(|| {
        Config::load_from_env().unwrap_or_else(|e| {
            panic!("FATAL - WHILE LOADING CONFIG - Cause: {e:?}");
        })
    })
}

#[allow(non_snake_case)]
pub struct Config {
    // -- Keys
    pub PWD_KEY: Vec<u8>,

    pub TOKEN_KEY: Vec<u8>,
    pub TOKEN_DURATION_SEC: f64,

    // -- Db
    pub DB_URL: String,

    // -- Web
    pub WEB_DIRECTORY: String,
}

impl Config {
    fn load_from_env() -> Result<Self> {
        Ok(Self {
            // -- Keys
            PWD_KEY: get_env_b64u_as_u8s("SERVICE_PWD_KEY")?,

            TOKEN_KEY: get_env_b64u_as_u8s("SERVICE_TOKEN_KEY")?,
            TOKEN_DURATION_SEC: get_env_parse("SERVICE_TOKEN_DURATION_SEC")?,

            // -- Db
            DB_URL: get_env("SERVICE_DB_URL")?,

            // -- Web
            WEB_DIRECTORY: get_env("SERVICE_WEB_DIRECTORY")?,
        })
    }
}
