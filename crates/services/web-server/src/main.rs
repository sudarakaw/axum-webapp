use axum::{middleware, Router};
use lib_core::{_dev_utils, model::ModelManager};
use tokio::net::TcpListener;
use tower_cookies::CookieManagerLayer;
use tracing::info;
use tracing_subscriber::EnvFilter;
use web_server::{
    error::Result,
    web::{self, middleware::require_ctx, route::rpc},
};

#[tokio::main]
async fn main() -> Result<()> {
    tracing_subscriber::fmt()
        .without_time() // For early local development to reduce output polution.
        .with_target(false)
        .with_env_filter(EnvFilter::from_default_env())
        .init();

    // FOR DEV ONLY
    _dev_utils::init_dev().await;

    // Initialize ModelManager
    let mm = ModelManager::new().await?;

    let routes_rpc = rpc::routes(mm.clone()).route_layer(middleware::from_fn(require_ctx));

    let routes = Router::new()
        .merge(web::route::login::routes(mm.clone()))
        .nest("/api", routes_rpc)
        .layer(middleware::map_response(web::middleware::response_map))
        .layer(middleware::from_fn_with_state(
            mm.clone(),
            web::middleware::ctx_resolve,
        ))
        .layer(CookieManagerLayer::new())
        .fallback_service(web::route::r#static::serve_dir());

    let listener = TcpListener::bind("127.0.0.1:5000").await.unwrap();
    info!("{:<12} - {:?}", "LISTENING", listener.local_addr());

    axum::serve(listener, routes.into_make_service())
        .await
        .unwrap();

    Ok(())
}
