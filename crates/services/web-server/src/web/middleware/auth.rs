use async_trait::async_trait;
use axum::{
    body::Body,
    extract::{FromRequestParts, State},
    http::{request::Parts, Request},
    middleware::Next,
    response::Response,
};
use lib_auth::token::{validate_web_token, Token};
use lib_core::{
    ctx::Ctx,
    model::{
        user::{UserBmc, UserForAuth},
        ModelManager,
    },
};
use serde::Serialize;
use tower_cookies::{Cookie, Cookies};
use tracing::debug;

use crate::web::{
    error::{Error, Result},
    set_token_cookie, AUTH_TOKEN,
};

pub type CtxExtResult = core::result::Result<CtxWrapper, CtxExtError>;

#[derive(Debug, Clone, Serialize)]
pub enum CtxExtError {
    TokenNotInCookie,
    TokenWrongFormat,

    UserNotFound,
    ModelAccessError(String),
    FailValidate,
    CannotSetTokenCookie,

    CtxNotInRequestExt,
    CtxCreateFaile(String),
}

pub async fn require_ctx(
    ctx: Result<CtxWrapper>,
    req: Request<Body>,
    next: Next,
) -> Result<Response> {
    debug!("{:<12} - middleware::require_ctx - {ctx:?}", "MIDDLEWARE");

    ctx?;

    Ok(next.run(req).await)
}

pub async fn ctx_resolve(
    mm: State<ModelManager>,
    cookies: Cookies,
    mut req: Request<Body>,
    next: Next,
) -> Result<Response> {
    debug!("{:<12} - middleware::ctx_resolve", "MIDDLEWARE");

    let ctx_ext_result = _ctx_resolve(mm, &cookies).await;

    if ctx_ext_result.is_err() && !matches!(ctx_ext_result, Err(CtxExtError::TokenNotInCookie)) {
        cookies.remove(Cookie::from(AUTH_TOKEN));
    }

    // Store the ctx_ext_result in the request extension (for Ctx exrtactor).
    req.extensions_mut().insert(ctx_ext_result);

    Ok(next.run(req).await)
}

async fn _ctx_resolve(mm: State<ModelManager>, cookies: &Cookies) -> CtxExtResult {
    // Get Token string
    let token = cookies
        .get(AUTH_TOKEN)
        .map(|c| c.value().to_string())
        .ok_or(CtxExtError::TokenNotInCookie)?;

    // Parse Token
    let token: Token = token.parse().map_err(|_| CtxExtError::TokenWrongFormat)?;

    // Get UserForAuth
    let user: UserForAuth = UserBmc::first_by_username(&Ctx::root_ctx(), &mm, &token.identity)
        .await
        .map_err(|e| CtxExtError::ModelAccessError(e.to_string()))?
        .ok_or(CtxExtError::UserNotFound)?;

    // Validate Token
    validate_web_token(&token, user.token_salt).map_err(|_| CtxExtError::FailValidate)?;

    // Update Token
    set_token_cookie(cookies, &user.username, user.token_salt)
        .map_err(|_| CtxExtError::CannotSetTokenCookie)?;

    // Create CtxExtResult
    Ctx::new(user.id)
        .map(CtxWrapper)
        .map_err(|e| CtxExtError::CtxCreateFaile(e.to_string()))
}

// Ctx extractor {{{

#[derive(Debug, Clone)]
pub struct CtxWrapper(pub Ctx);

#[async_trait]
impl<S: Send + Sync> FromRequestParts<S> for CtxWrapper {
    type Rejection = Error;

    async fn from_request_parts(parts: &mut Parts, _state: &S) -> Result<Self> {
        debug!("{:<12} - Ctx", "EXTRACTOR");

        parts
            .extensions
            .get::<CtxExtResult>()
            .ok_or(Error::CtxExt(CtxExtError::CtxNotInRequestExt))?
            .clone()
            .map_err(Error::CtxExt)
    }
}

// }}}
