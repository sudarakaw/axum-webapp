use std::sync::Arc;

use axum::{
    http::{Method, Uri},
    response::{IntoResponse, Response},
    Json,
};
use lib_rpc::RpcInfo;
use serde_json::{json, to_value};
use tracing::debug;
use uuid::Uuid;

use crate::{log::log_request, web::Error};

use super::CtxWrapper;

pub async fn response_map(
    ctx: Option<CtxWrapper>,
    uri: Uri,
    req_method: Method,
    res: Response,
) -> Response {
    let ctx = ctx.map(|ctx| ctx.0);

    debug!("{:<12} - middleware::response_map", "RES_MAPPER");

    let uuid = Uuid::new_v4();

    let rpc_info = res.extensions().get::<RpcInfo>();

    // Get the eventual response error
    let web_error = res.extensions().get::<Arc<Error>>();
    let client_status_error = web_error.map(|e| e.client_status_and_error());

    // If client error, build the new response.
    let error_response = client_status_error
        .as_ref()
        .map(|(status_code, client_error)| {
            let client_error = to_value(client_error).ok();
            let message = client_error.as_ref().and_then(|v| v.get("message"));
            let detail = client_error.as_ref().and_then(|v| v.get("detail"));

            let client_error_body = json!({
                "id": rpc_info.as_ref().map(|rpc| rpc.id.clone()),
                "error": {
                    "message": message, // Variant name from web::Error
                    "data": {
                        "request_uuid": uuid.to_string(),
                        "detail": detail
                    }
                }
            });

            debug!("CLIENT ERROR_BODY:\n{client_error_body}");

            // Build the new response from client_error_body
            (*status_code, Json(client_error_body)).into_response()
        });

    // Build and log the server log line
    let client_error = client_status_error.unzip().1;

    let _ = log_request(
        uuid,
        req_method,
        uri,
        rpc_info,
        ctx,
        web_error,
        client_error,
    )
    .await;

    debug!("\n");

    error_response.unwrap_or(res)
}
