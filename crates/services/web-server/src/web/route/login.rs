use axum::{extract::State, routing, Json, Router};
use lib_auth::pwd::{self, ContentToHash, SchemeStatus};
use lib_core::{
    ctx::Ctx,
    model::{
        user::{UserBmc, UserForLogin},
        ModelManager,
    },
};
use serde::Deserialize;
use serde_json::{json, Value};
use tower_cookies::Cookies;
use tracing::debug;

use crate::web::{remove_token_cookie, set_token_cookie, Error, Result};

pub fn routes(mm: ModelManager) -> Router {
    Router::new()
        .route("/api/login", routing::post(api_login))
        .route("/api/logoff", routing::post(api_logoff))
        .with_state(mm)
}

// {{{ Login

#[derive(Debug, Deserialize)]
struct LoginPayload {
    username: String,
    password: String,
}

async fn api_login(
    State(mm): State<ModelManager>,
    cookies: Cookies,
    Json(payload): Json<LoginPayload>,
) -> Result<Json<Value>> {
    debug!("{:<12} - api_login", "HANDLER");

    let LoginPayload {
        username,
        password: pwd_clear,
    } = payload;
    let root_ctx = Ctx::root_ctx();

    // Get the user.
    let user: UserForLogin = UserBmc::first_by_username(&root_ctx, &mm, &username)
        .await?
        .ok_or(Error::LoginFailUsernameNotFound)?;
    let user_id = user.id;

    // Validate the password
    let Some(pwd) = user.pwd else {
        return Err(Error::LoginFailUserHasNoPwd { user_id });
    };

    let scheme_status = pwd::validate(
        ContentToHash {
            content: pwd_clear.clone(),
            salt: user.pwd_salt,
        },
        pwd,
    )
    .await
    .map_err(|_| Error::LoginFailPwdNotMatching { user_id })?;

    // Update password scheme if needed
    if let SchemeStatus::Outdated = scheme_status {
        debug!("password encrypt scheme outdated, upgrading.");

        UserBmc::update_pwd(&root_ctx, &mm, user_id, &pwd_clear).await?;
    }

    // Set web token
    set_token_cookie(&cookies, &user.username, user.token_salt)?;

    let body = Json(json!({
        "result": {
            "success":true
        }
    }));

    Ok(body)
}

// }}}

// {{{ Logoff

#[derive(Debug, Deserialize)]
struct LogoffPayload {
    logoff: bool,
}

async fn api_logoff(cookies: Cookies, Json(payload): Json<LogoffPayload>) -> Result<Json<Value>> {
    debug!("{:<12} - api_logoff", "HANDLER");

    let should_logoff = payload.logoff;

    if should_logoff {
        remove_token_cookie(&cookies)?;
    }

    // Create the success body
    let body = Json(json!({
        "result": {
            "logged_off": should_logoff
        }
    }));

    Ok(body)
}

// }}}
