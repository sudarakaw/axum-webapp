use axum::{
    extract::State,
    response::{IntoResponse, Response},
    routing::post,
    Json, Router,
};
use lib_core::{ctx::Ctx, model::ModelManager};
use lib_rpc::{execute_rpc, RpcInfo, RpcRequest};
use serde_json::{json, Value};
use tracing::debug;

use crate::web::{middleware::CtxWrapper, Result};

pub fn routes(mm: ModelManager) -> Router {
    Router::new()
        .route("/rpc", post(route_handler))
        .with_state(mm)
}

async fn route_handler(
    State(mm): State<ModelManager>,
    ctx: CtxWrapper,
    Json(req): Json<RpcRequest>,
) -> Response {
    let ctx = ctx.0;

    // Create the RPC Info to be set to the response.extensions.
    let rpc_info = RpcInfo {
        id: req.id.clone(),
        method: req.method.clone(),
    };

    // Execute & Store RpcInfo in response
    let mut response = _route_handler(mm, ctx, req).await.into_response();

    response.extensions_mut().insert(rpc_info);

    response
}

async fn _route_handler(mm: ModelManager, ctx: Ctx, req: RpcRequest) -> Result<Json<Value>> {
    let req_id = req.id.clone();
    let req_method = req.method.clone();

    debug!(
        "{:<12} - rpc::_route_handler - method {req_method}",
        "HANDLER"
    );

    let result_json = execute_rpc(mm, ctx, req).await?;

    let body = json!({
        "id": req_id,
        "result": result_json
    });

    Ok(Json(body))
}
